FROM node:12.16.2:alpine

# APP DIRECTORY 
WORKDIR /usr/src/app

# APP DEPENDENCIES
COPY package*.json ./

RUN npm install

COPY . .

# BIND PORT APP

EXPOSE 3000

# NON-ROOT
USER node

CMD [ "node", "index.js" ] 
